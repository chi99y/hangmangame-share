//
//  LetterPickerViewController.m
//  HangMan
//
//  Created by Chin S. Huang on 2/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LetterPickerViewController.h"
#import "SecretWord.h"
#import "CustomButton.h"

@interface LetterPickerViewController()
@property (nonatomic,strong) NSArray *letterArray;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (weak, nonatomic) IBOutlet CustomButton *selectButton;
@end


//private constants
CGPoint const SELECTBTN_PORTRAIT= {160.0f, 375.0f};
CGPoint const SELECTBTN_LANDSCAPE= {240.0f, 270.0f};

@implementation LetterPickerViewController

@synthesize selectedLetter = _selectedLetter, letterArray = _letterArray, pickerView = _pickerView, selectButton = _selectButton, delegate = _delegate;


- (IBAction)selectLetter:(id)sender 
{
    //just set the letter according to the current row of the picker.
    self.selectedLetter = [self.letterArray objectAtIndex:[self.pickerView selectedRowInComponent:0]];
    
     NSLog(@"SelectedLetter :: %@", self.selectedLetter);
    
    //communicate the selection to delegate
    [self.delegate letterPickerView:self didSelect:self.selectedLetter];

}


#pragma mark - View lifecycle

- (void)viewDidLoad
{
    //set the button text
    [self.selectButton setTitle:@"Select" forState:UIControlStateNormal];
    
    //populate the array with remaining letters
    self.letterArray = [self.delegate setRemainingLettersFor:self];
    
}

- (void)viewDidUnload
{
    [self setPickerView:nil];
    [self setSelectButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait || 
        toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        //move gallows to portrait position 
        self.selectButton.center = SELECTBTN_PORTRAIT;
    }
    else 
    {
        //move gallows to landscape position
        self.selectButton.center = SELECTBTN_LANDSCAPE;
    }
    
}

#pragma mark - Implementing UIPickerView Protocols


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [self.letterArray count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSLog(@"pickerView ::letterArray :: titleForRow =%@", [self.letterArray objectAtIndex:row] );
    return [self.letterArray objectAtIndex:row];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return 80.0;
}



@end
