//
//  SecretWordDisplayView.h
//  HangManGame
//
//  Created by Chin S. Huang on 3/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecretWordDisplayView : UIControl

- (void)displaySecretWordWith:(NSString *)delimitedString;

- (void)showCorrectGuessAnimation;

- (void)showInCorrectGuessAnimation;

- (void)clearDisplay;

@end
