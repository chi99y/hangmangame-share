//
//  HangingManView.m
//  HangMan
//
//  Created by Chin S. Huang on 2/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HangingManView.h"

//constants
float const LINE_WIDTH = 5.0;
float const HEAD_RADIUS = 25.0;
CGPoint const HEAD_CENTER = {50.0f, 29.0f};
CGPoint const TORSO_BEGIN = {50.0f,55.0f};
CGPoint const TORSO_END = {50.0f,135.0f};
CGPoint const LEFTARM_BEGIN = {50.0f,70.0f};
CGPoint const LEFTARM_END = {10.0f,105.0f};
CGPoint const RIGHTARM_BEGIN = {50.0f,70.0f};
CGPoint const RIGHTARM_END = {90.0f,105.0f};
CGPoint const LEFTLEG_BEGIN = {50.0f,135.0f};
CGPoint const LEFTLEG_END = {20.0f,185.0f};
CGPoint const RIGHTLEG_BEGIN = {50.0f,135.0f};
CGPoint const RIGHTLEG_END = {80.0f,185.0f};

@interface HangingManView() 
@property (nonatomic, strong) UIColor *lineColor;
@end

@implementation HangingManView

@synthesize lineColor = _lineColor, dataSource = _dataSource;

- (void)setup
{
    //set color
    self.lineColor = [UIColor blueColor];
}

- (void)awakeFromNib
{
    [self setup];
}

- (void)drawHead:(CGContextRef)context
{
    UIGraphicsPushContext(context);
    CGContextBeginPath(context);
    CGContextAddArc(context, HEAD_CENTER.x, HEAD_CENTER.y, HEAD_RADIUS, 0, 2*M_PI, YES);
    CGContextStrokePath(context);
    UIGraphicsPopContext();
}

- (void)drawLineFrom:(CGPoint)beginPoint toEndPoint:(CGPoint)endPoint inContext:(CGContextRef)context
{
    UIGraphicsPushContext(context);
    CGContextMoveToPoint(context, beginPoint.x, beginPoint.y);
    CGContextAddLineToPoint(context, endPoint.x, endPoint.y);
    CGContextStrokePath(context);
    UIGraphicsPopContext();
}

- (void)drawHangManIn:(int)stage inContext:(CGContextRef)context
{
    //clear graphics -- good if context is set to 0 or less
    CGContextClearRect(context, self.frame);
    
    if (stage >= 1 ){
        //draw head
        [self drawHead:context];
    }
    if (stage >= 2 ){
        //draw torso
        [self drawLineFrom:TORSO_BEGIN toEndPoint:TORSO_END inContext:context];
    }
    if (stage >= 3 ){
        //draw left arm
        [self drawLineFrom:LEFTARM_BEGIN toEndPoint:LEFTARM_END inContext:context];
    }
    if (stage >= 4 ){
        //draw right arm
        [self drawLineFrom:RIGHTARM_BEGIN toEndPoint:RIGHTARM_END inContext:context];
    }
    if (stage >= 5 ){
        //draw left leg
        [self drawLineFrom:LEFTLEG_BEGIN toEndPoint:LEFTLEG_END inContext:context];
    }
    if (stage >= 6 ){
        //draw right leg
        [self drawLineFrom:RIGHTLEG_BEGIN toEndPoint:RIGHTLEG_END inContext:context];
    }
    if (stage > 6 ){
        //error
        NSLog(@"HangingManView :: Error--there are only 6 stages");
    }
    
}

- (void)drawRect:(CGRect)rect
{
    //reference context
    CGContextRef context = UIGraphicsGetCurrentContext();
    //set line width
    CGContextSetLineWidth(context, LINE_WIDTH);
    //set line color
    [self.lineColor setStroke];
    
    //main drawing api
    [self drawHangManIn:[self.dataSource getCurrentHangStage:self] inContext:context];
    
}


- (NSString *)description
{
    return @"[HangingManView: Display the Hanging Man with Drawing in Stages]";
}

@end
