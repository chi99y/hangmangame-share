//
//  HangingManView.h
//  HangMan
//
//  Created by Chin S. Huang on 2/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HangingManView;

@protocol HangingManViewDataSource <NSObject>

- (NSUInteger)getCurrentHangStage:(HangingManView *)sender;

@end

@interface HangingManView : UIView
@property (nonatomic, weak) id<HangingManViewDataSource>dataSource;
@end
