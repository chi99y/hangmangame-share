//
//  SecretWord.h
//  HangManGame
//
//  Created by Chin S. Huang on 2/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

//public constants
extern NSUInteger const MINCHAR_SECRETWORD;
extern NSUInteger const MAXCHAR_SECRETWORD;

@interface SecretWord : NSObject <NSCopying>

+ (NSArray *)permissibleLetters;//in this case the English alphabet
+ (BOOL)testIfLetterIsPermissible:(NSString *)letter;

- (BOOL)setSecretWord:(NSString *)aWord; //will be validated against permissible letters

- (NSString *)wordAsString;//returns word as a string

- (NSArray *)wordAsArray;//return array of uppercase characters for the word

- (id)copyWithZone:(NSZone *)zone;

@end
