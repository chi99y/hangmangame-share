//
//  HangManGame.h
//  HangMan
//
//  Created by Chin S. Huang on 2/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SecretWord.h"

//enums
typedef enum {
    SecretWordNotSet = -2,
    InvalidGuess = -1,
    GuessIncorrect = 0,
    GuessCorrect = 1,
    YouWin = 2,
    GameOver = 6
} GuessResult;

//constants
extern NSUInteger const ALLOWED_MISSES;

@interface HangManGame : NSObject
@property (nonatomic,strong, readonly) SecretWord *secretWord;
@property (nonatomic,strong, readonly) NSMutableArray *matchedLetterPositions;
@property (nonatomic,strong,readonly) NSMutableArray *remainingLetters;
@property (nonatomic,strong,readonly) NSMutableArray *guessedLetters;
@property (nonatomic, strong, readonly) NSString *currentMissedLetter;
@property (nonatomic,readonly) NSUInteger missed;
@property (nonatomic,readonly) BOOL isGameOver;
@property (nonatomic) GuessResult gameResult;

+ (NSString *)stringFromArray:(NSArray *)anArray;

- (id)initGameWithSecretWord:(SecretWord *)secretWord;
- (GuessResult)guessALetter:(NSString *)aLetter;
- (GuessResult)guessTheWord:(NSString *)aWord;
- (NSString *)getGuessedLettersAsString;
- (NSString *)getRemainingLettersAsString;

@end
