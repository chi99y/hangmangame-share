//
//  GameOverViewController.m
//  HangManGame
//
//  Created by Chin S. Huang on 3/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GameOverViewController.h"
#import "GameOverOverlayView.h"
#import "CustomButton.h"

@interface GameOverViewController() 

@property (weak, nonatomic) IBOutlet GameOverOverlayView *gameOverView;

@property (weak, nonatomic) IBOutlet CustomButton *restartButton;
@end

@implementation GameOverViewController
@synthesize gameOverView = _gameOverView;
@synthesize restartButton = _restartButton;

@synthesize delegate = _delegate, viewMode = _viewMode;


#pragma mark - View lifecycle

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //display graphics
    if (self.viewMode == ShowWinView)
    {
        [self.gameOverView displayWinningGraphics];
         
    } else {
        [self.gameOverView displayLosingGraphics];
    }
    
    //style button
    [self.restartButton setTitle:@"Back" forState:UIControlStateNormal];
}


- (void)viewDidUnload
{
    [self setGameOverView:nil];
    [self setRestartButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

#pragma - handle action

- (IBAction)restartGameAction:(id)sender 
{
    [self.delegate gameOverViewDidRestartGame:self];
}



@end
