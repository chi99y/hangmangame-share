//
//  SecretWordViewController.m
//  HangManGame
//
//  Created by Chin S. Huang on 2/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SecretWordChooserViewController.h"

//private constants
CGPoint const LABEL_PORTRAIT= {160.0f, 110.0f};
CGPoint const LABEL_LANDSCAPE= {240.0f, 60.0f};
CGPoint const TEXTFIELD_PORTRAIT= {160.0f, 180.0f};
CGPoint const TEXTFIELD_LANDSCAPE= {240.0f, 100.0f};
NSString *const SECRETWORD_MODE_LABELTEXT = @"Type your secret word to start playing.";
NSString *const GUESSWORD_MODE_LABELTEXT = @"Type your guess below.";
NSString *const INVALID_LETTER_ERROR_MESSAGE = @"Only letters of the alphabet is permitted.";
NSString *const WORD_EXCEEDED_MAX_LIMIT = @"You've reached the max limit of 15 letters.";


@interface SecretWordChooserViewController()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UITextField *textField;

@end

@implementation SecretWordChooserViewController

@synthesize secretWord = _secretWord, guessedWord = _guessedWord, delegate = _delegate, viewMode = _viewMode;//public 
@synthesize label = _label, textField = _textField;//private


#pragma mark - private methods

- (void)setLabelTextForErrorMessage:(NSString *)message
{
    self.label.text = message;
    self.label.textColor = [UIColor redColor];
    
}

- (void)restoreLabelText
{
    //restore text
    if (self.viewMode == ChooseSecretWord) {
        self.label.text = SECRETWORD_MODE_LABELTEXT;
    } else {
        self.label.text = GUESSWORD_MODE_LABELTEXT;
    }
    
    //restore color
    self.label.textColor = [UIColor blackColor];
}


#pragma mark - implement UITextFieldDelegate protocol

//validate each character to make sure they are permissible
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSLog(@"textfield entry :: range(%i) : string.length(%i) : string(%@)", range.length, string.length, string);
    
    //reset message
    [self restoreLabelText];
    
    //validate each character
    
    if (range.length >0 && string.length==0)
    {
        //user is deleting which is permitted
        return YES;
    }
    
    if (![SecretWord testIfLetterIsPermissible:string])
    {
        //show error message
        [self setLabelTextForErrorMessage:INVALID_LETTER_ERROR_MESSAGE];
        return NO;
    } 
    
    if ([textField.text length] > MAXCHAR_SECRETWORD)
    {
        //show error message
        [self setLabelTextForErrorMessage:WORD_EXCEEDED_MAX_LIMIT];
        return NO;
    }
    
    NSLog(@"textfield entry :: permissible=%@", string);
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (self.viewMode == ChooseSecretWord)
    {
        if ([[self.secretWord wordAsString] length]>0)
        {
            [self.delegate secretWordController:self didSetSecretWord:self.secretWord];
            
            return;
        }
        
    } else {
        
        if (self.textField.text.length > 0)
        {
            [self.delegate secretWordController:self didSetGessedWord:self.guessedWord];
            
            return;
        }
    }
    
    //if returned without setting the secret word or guessed word we'll return back
    [[self presentingViewController] dismissModalViewControllerAnimated:YES];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.text.length > 0 )
    {
        if (self.viewMode == ChooseSecretWord)
        {
            [self.secretWord setSecretWord:textField.text];
            
        } else {
            
            self.guessedWord = textField.text;
        }
        
        [textField resignFirstResponder];
        
        return YES;
        
    } else {
        
        return NO;
    }
    
}


#pragma mark - View lifecycle

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    //create a new instance of SecretWord object
    self.secretWord = [[SecretWord alloc] init];
    
    //set the UITextField delegate to this controller
    self.textField.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //set the correct message to user
    [self restoreLabelText];
    
    //prompt the keyboard to appear
    [self.textField becomeFirstResponder];
}

- (void)viewDidUnload {
    self.textField = nil;
    self.label = nil;
    self.secretWord = nil;
    [super viewDidUnload];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait || 
        toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        self.label.center = LABEL_PORTRAIT;
        self.textField.center = TEXTFIELD_PORTRAIT;
    }
    else 
    {
        self.label.center = LABEL_LANDSCAPE;
        self.textField.center = TEXTFIELD_LANDSCAPE;
    }
}


@end
