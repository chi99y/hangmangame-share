//
//  GuessLetter.m
//  HangManGame
//
//  Created by Chin S. Huang on 3/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GuessLetter.h"
#import "DrawingUtil.h"
#import <QuartzCore/QuartzCore.h>

@interface GuessLetter()
@property (nonatomic, strong) CALayer *textlayer;
@property (nonatomic, strong) CAShapeLayer *lineLayer;
@property (nonatomic, strong) DrawTextToLayerDelegate *textLayerDelegate;
@end

#define FADE_OPACITY .4

@implementation GuessLetter

@synthesize letter = _letter, letterState = _letterState, textlayer = _textlayer, lineLayer = _lineLayer, textLayerDelegate = _textLayerDelegate;



- (void)clearLayers
{
    for (CALayer *sublayers in self.layer.sublayers)
    {
        [sublayers removeFromSuperlayer];
    }
}

- (void)createTextLayer
{
    //clear first
    [self clearLayers];
    
    CALayer *rootLayer = self.layer;
    self.textlayer = [CALayer layer];
    self.textlayer.frame = self.bounds;
    self.textlayer.anchorPoint = CGPointMake(.5, .5);
    self.textlayer.position = CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds)+3);
    self.textlayer.delegate = self.textLayerDelegate;
    [self.textlayer setNeedsDisplay];
    
    [rootLayer addSublayer:self.textlayer]; 
}

- (void)drawLineLayer
{
    CALayer *rootLayer = self.layer;
    self.lineLayer = [CAShapeLayer layer];
    self.lineLayer.frame = self.bounds;
    
    //create path
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:CGPointMake(self.bounds.size.width, 0)];
    [path addLineToPoint:CGPointMake(0, self.bounds.size.height)];
    [path closePath];
    
    self.lineLayer.path = [path CGPath];
    self.lineLayer.lineWidth = 3;
    self.lineLayer.strokeColor = [UIColor redColor].CGColor;
    self.lineLayer.masksToBounds = YES;
    
    [rootLayer addSublayer:self.lineLayer];
}

- (void)fadeTextOverDuration:(float)duration withLine:(BOOL)drawLine
{
    //set the final opacity
    self.textlayer.opacity = FADE_OPACITY;
    
    //run the animation
    CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:@"opacity"];
    anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    anim.duration = duration;
    anim.repeatCount = 1;
    anim.fromValue = [NSNumber numberWithFloat:1.0];
    anim.toValue = [NSNumber numberWithFloat:FADE_OPACITY];
    anim.delegate = drawLine ? self : nil;
    [self.textlayer addAnimation:anim forKey:@"animateOpacity"];
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    //draw the line
    [self drawLineLayer];
}


#pragma -public methods

- (id)initWithLetter:(NSString *)aLetter inFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _letter = aLetter;
        
        //create a delegate
        self.textLayerDelegate = [[DrawTextToLayerDelegate alloc] initWithLetter:_letter];
        
        //create the text layer
        [self createTextLayer];
    }
    return self;
}

- (void)setState:(GuessLetterState)state
{
    if (state == MissedLetterState)
    {
        [self createTextLayer];
        [self fadeTextOverDuration:.5 withLine:YES];
        
    } 
    else if (state == GuessedLetterState)
    {
        [self createTextLayer];
        [self fadeTextOverDuration:.5 withLine:NO];
    }
    else
    {
        [self createTextLayer];
    }
}


- (void)setLetterState:(GuessLetterState)letterState
{
    _letterState = letterState;
    
    [self setNeedsDisplay];
}

@end

@interface DrawTextToLayerDelegate()
@property (nonatomic,strong) NSString *myLetter;
@end

@implementation DrawTextToLayerDelegate

@synthesize myLetter = _myLetter;

- (id)initWithLetter:(NSString *)aLetter
{
    if (self = [super init])
    {
        //initialize
        self.myLetter = aLetter;
    }
    
    return self;
}

- (void)drawLayer:(CALayer *)layer inContext:(CGContextRef)ctx
{

    //use the DrawingUtils to draw text into context
    [DrawingUtil drawTextInContext:ctx 
                            inRect:layer.frame
                          withText:self.myLetter
                      withFontSize:18 
                     letterSpacing:2
                             color:[UIColor blackColor] 
                       drawingMode:kCGTextFill];
    
}
@end
