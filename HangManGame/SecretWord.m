//
//  SecretWord.m
//  HangManGame
//
//  Created by Chin S. Huang on 2/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SecretWord.h"

//constant initialization
NSUInteger const  MINCHAR_SECRETWORD = 2;
NSUInteger const  MAXCHAR_SECRETWORD = 15;

@interface SecretWord()
@property (nonatomic, strong) NSString *theWord;
@property (nonatomic, strong) NSArray *theWordArray;
@end

@implementation SecretWord

@synthesize theWord = _theWord, theWordArray = _theWordArray;


//Class method list of all the letters of the alphabet
+ (NSArray *)permissibleLetters
{
    return [[NSArray alloc] initWithObjects:
            @"A",@"B",@"C",@"D",@"E",@"F",@"G",@"H",@"I",@"J",@"K",@"L",@"M",
            @"N",@"O",@"P",@"Q",@"R",@"S",@"T",@"U",@"V",@"W",@"X",@"Y",@"Z",
            nil];
}

+ (BOOL)testIfLetterIsPermissible:(NSString *)aLetter
{
    return [[SecretWord permissibleLetters] containsObject:[aLetter uppercaseString]];
}

#pragma private methods

- (NSMutableArray *)convertStringToMutableArray:(NSString *)aString
{
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    for (int i=0; i < [aString length]; i++) {
        NSString *letter  = [NSString stringWithFormat:@"%c", [aString characterAtIndex:i]];
        [arr addObject:[letter uppercaseString]];
    }
    
    return arr;
}

- (BOOL)validateTheWord:(NSString *)word
{
    //check for minimum and maximum number of characters
    if ( word == nil ||
        [word length] < MINCHAR_SECRETWORD||
        [word length] > MAXCHAR_SECRETWORD)
    {
        return NO;
    }
    
    //convert word to array then compare each characters to alphabet
    NSMutableArray *arr = [self convertStringToMutableArray:word];
    for (int i=0; i<[arr count]; i++) {
        if (![[SecretWord permissibleLetters] containsObject:[arr objectAtIndex:i]])
        {
            return NO;
        }
    }
    
    return YES;
}

#pragma public methods

- (BOOL)setSecretWord:(NSString *)aWord
{
    if (aWord != _theWord && [self validateTheWord:aWord])
    {
        //break the characters up into an array
        _theWordArray = [self convertStringToMutableArray:aWord];
        
        _theWord = aWord;
        
    } else {
        return NO;
    }
    
    return YES;
}

- (NSString *)wordAsString
{
    return self.theWord;
}

- (NSArray *)wordAsArray
{
    return self.theWordArray;
}

#pragma implement copying protocol

- (id)copyWithZone:(NSZone *)zone
{
    SecretWord *copy = [[SecretWord alloc] init];
    [copy setSecretWord:self.theWord];
    
    return copy;
}


@end
