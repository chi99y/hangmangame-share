//
//  CustomButton.m
//  HangManGame
//
//  Created by Chin S. Huang on 2/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CustomButton.h"

@implementation CustomButton


- (void)awakeFromNib
{
    //style the selection button
    UIImage *buttonImage = [[UIImage imageNamed:@"blackButton.png"]
                            resizableImageWithCapInsets:UIEdgeInsetsMake(18, 18, 18, 18)];
    UIImage *buttonImageHighlight = [[UIImage imageNamed:@"blackButtonHighlight.png"]
                                     resizableImageWithCapInsets:UIEdgeInsetsMake(18, 18, 18, 18)];
    // Set the background for any states you plan to use
    [self setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [self setBackgroundImage:buttonImageHighlight forState:UIControlStateHighlighted];
    
    //set the color with UIButton api
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}



@end
