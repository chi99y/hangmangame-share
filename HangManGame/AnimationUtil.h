//
//  AnimationUtil.h
//  HangManGame
//
//  Created by Chin S. Huang on 3/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AnimationUtil : NSObject

+ (void)animateLayer:(CALayer *)layer withColor:(CGColorRef)color toDelegate:(id)delegate;

@end
