//
//  AnimationUtil.m
//  HangManGame
//
//  Created by Chin S. Huang on 3/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AnimationUtil.h"
#import <QuartzCore/QuartzCore.h>

@implementation AnimationUtil

+ (void)animateLayer:(CALayer *)layer withColor:(CGColorRef)color toDelegate:(id)delegate
{
    CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:@"backgroundColor"];
    anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    anim.duration = .2;
    anim.repeatCount = 1;
    anim.autoreverses = YES;
    anim.removedOnCompletion = YES;
    anim.toValue = (__bridge id)color;
    anim.delegate = delegate;
    [layer addAnimation:anim forKey:nil];
}

@end
