//
//  GuessLetterContainerView.h
//  HangManGame
//
//  Created by Chin S. Huang on 3/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GuessLetterContainerView : UIControl

- (void)resetLetters;
- (void)setMissedLetter:(NSString *)aLetter;
- (void)setGuessedLetter:(NSString *)aLetter;

@end
