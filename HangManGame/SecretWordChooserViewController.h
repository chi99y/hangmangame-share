//
//  SecretWordViewController.h
//  HangManGame
//
//  Created by Chin S. Huang on 2/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SecretWord.h"

@protocol SecretWordChooserViewControllerDelegate <NSObject>

- (void)secretWordController:(id)sender didSetSecretWord:(SecretWord *)theWord;

- (void)secretWordController:(id)sender didSetGessedWord:(NSString *)theWord;

@end

//enums
typedef enum {
    ChooseSecretWord,
    GuessSecretWord
} SecretWordChooserMode;

@interface SecretWordChooserViewController : UIViewController

@property (nonatomic, strong) SecretWord *secretWord;
@property (nonatomic, strong) NSString *guessedWord;
@property (nonatomic) SecretWordChooserMode viewMode;
@property (nonatomic, weak) id<SecretWordChooserViewControllerDelegate> delegate;

@end
