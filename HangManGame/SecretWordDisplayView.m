//
//  SecretWordDisplayView.m
//  HangManGame
//
//  Created by Chin S. Huang on 3/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SecretWordDisplayView.h"
#import <QuartzCore/QuartzCore.h>
#import "DrawingUtil.h"

@interface SecretWordDisplayView()
@property (nonatomic, strong) NSString *secretWordText;
@property (nonatomic, strong) CALayer *bgLayer;
@end

@implementation SecretWordDisplayView

@synthesize secretWordText = _secretWordText, bgLayer = _bgLayer;

#define BG_COLOR_DEFAULT    [UIColor colorWithRed:0 green:.6 blue:1.0 alpha:.2].CGColor
#define BG_COLOR_CORRECT_GUESS  [UIColor greenColor].CGColor
#define BG_COLOR_WRONG_GUESS    [UIColor redColor].CGColor
#define BG_COLOR_ROLLOVER       [UIColor darkGrayColor].CGColor

#pragma - Drawing methods

- (void)clearLayers
{
    for (CALayer *sublayers in self.layer.sublayers)
    {
        [sublayers removeFromSuperlayer];
    }
}

- (void)drawRoundedRectAroundText:(CGRect)rect cornerRadius:(CGFloat)radius posX:(CGFloat)x posY:(CGFloat)y
{   
    [self clearLayers];
    
    self.bgLayer = [CALayer layer];
    self.bgLayer.cornerRadius = radius;
    self.bgLayer.frame = rect;
    self.bgLayer.backgroundColor = BG_COLOR_DEFAULT;
    self.bgLayer.anchorPoint = CGPointMake(0, 0);
    self.bgLayer.position = CGPointMake(x, y);
    
    [self.layer addSublayer:self.bgLayer];
}

- (void)animateBgLayerColorWith:(CGColorRef)targetColor
{
    CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:@"backgroundColor"];
    anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    anim.duration = .2;
    anim.repeatCount = 1;
    anim.autoreverses = YES;
    anim.removedOnCompletion = YES;
    anim.toValue = (__bridge id)targetColor;
    [self.bgLayer addAnimation:anim forKey:nil];
}

- (void)drawText:(CGContextRef)context inRect:(CGRect)rect
{
    //use the DrawingUtils to draw text into context
    DrawTextProps textProps = [DrawingUtil drawTextInContext:context inRect:rect withText:self.secretWordText withFontSize:18 letterSpacing:2 color:[UIColor blackColor] drawingMode:kCGTextFill];
    
    //only draw when we have text
    if (textProps.textWidth > 0)
    {
        float padding = 10.0;
        CGRect textRect = CGRectMake(0, 0, textProps.textWidth + padding, 28);
        [self drawRoundedRectAroundText:textRect cornerRadius:10 posX:(textProps.textPosX-padding) posY:0]; 
    } else {
        [self clearLayers];
    }
    
}



//main drawing method
- (void)drawRect:(CGRect)rect
{
    //reference context
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    [self drawText:context inRect:rect];
    
}

#pragma - touch events handling

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    //need to call super so UIControl events can be dispatched for other touch events
    [super touchesBegan:touches withEvent:event];
    
    self.bgLayer.backgroundColor = BG_COLOR_ROLLOVER;
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    
    self.bgLayer.backgroundColor = BG_COLOR_DEFAULT;
}

#pragma - public methods

- (void)displaySecretWordWith:(NSString *)delimitedString
{
    self.secretWordText = delimitedString;
    
    [self setNeedsDisplay];
}

- (void)showCorrectGuessAnimation
{
    [self animateBgLayerColorWith:BG_COLOR_CORRECT_GUESS];
}

- (void)showInCorrectGuessAnimation
{
    [self animateBgLayerColorWith:BG_COLOR_WRONG_GUESS];
}

- (void)clearDisplay
{
    [self displaySecretWordWith:@""];
}


@end
