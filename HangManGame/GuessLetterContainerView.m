//
//  GuessLetterContainerView.m
//  HangManGame
//
//  Created by Chin S. Huang on 3/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GuessLetterContainerView.h"
#import <QuartzCore/QuartzCore.h>
#import "GuessLetter.h"
#import "SecretWord.h"
#import "AnimationUtil.h"


@interface GuessLetterContainerView()
@property (nonatomic, assign) NSString *currentLetter;
@property (nonatomic) BOOL correctLetter;
@end

#define LETTER_SIZE 20
#define PADDING_SIZE 10
#define BG_COLOR_DEFAULT      [UIColor whiteColor].CGColor
#define BG_COLOR_ROLLOVER     [UIColor darkGrayColor].CGColor
#define BG_COLOR_WRONG_GUESS    [UIColor redColor].CGColor
#define BG_COLOR_CORRECT_GUESS    [UIColor greenColor].CGColor

@implementation GuessLetterContainerView

@synthesize currentLetter = _currentLetter, correctLetter = _correctLetter;

- (int)getMaxLettersPerRow
{
    return floor((self.bounds.size.width - (2*PADDING_SIZE)) / LETTER_SIZE);
}

- (void)styleContainer
{
    CALayer *rootLayer = self.layer;
    rootLayer.cornerRadius = 10;
    rootLayer.backgroundColor = BG_COLOR_DEFAULT;
    rootLayer.shadowColor = [UIColor blackColor].CGColor;
    rootLayer.shadowOffset = CGSizeMake(2, 2);
    rootLayer.shadowOpacity = .8;
}

- (void)removeAllSubViews
{
    if ( [self subviews].count > 0 )
    {
        for (UIView *view in self.subviews) {
            [view removeFromSuperview];
        }
    }
}

- (void)createLetters
{
    //clear
    [self removeAllSubViews];
    
    //NSLog(@"***** getMaxLetterPerRow = %i", [self getMaxLettersPerRow]);
    int itemCount = 0;
    int rowCount = 0;
    int tag = 1;//start with 1 since the self starts with 0
    for (NSString *element in [SecretWord permissibleLetters]) {
        CGRect letterRect = CGRectMake((itemCount*LETTER_SIZE) + PADDING_SIZE, (rowCount * LETTER_SIZE) + PADDING_SIZE, LETTER_SIZE, LETTER_SIZE);
        //NSLog(@"*** vowel [%@] placed in %f , counter = %i", element, letterRect.origin.x, itemCount);
        GuessLetter *letter = [[GuessLetter alloc] initWithLetter:element inFrame:letterRect];
        letter.tag = tag;
        letter.userInteractionEnabled = NO;//need to set this so subviews don't interrupt container's button actions
        [self addSubview:letter];
        itemCount++;
        tag++;
        //track itemCount to make sure it doesn't go over maximum
        if ((itemCount % ([self getMaxLettersPerRow])) == 0) {
            rowCount++;
            itemCount = 0;
        }
        
    }
}

- (void)changeLetterState:(NSString *)aLetter toState:(GuessLetterState)state
{
    //get the letter index
    int index = [[SecretWord permissibleLetters] indexOfObject:aLetter];
    //get the view from index
    GuessLetter *theLetter = (GuessLetter *)[self viewWithTag:index+1];
    
    if (theLetter)
    {
        //change the view's state
        [theLetter setState:state]; 
    } else {
        NSLog(@"** GuessLetterContainerView :: changeLetterState :: ERROR--letter cannot be found!");
    }
    
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        //initialize code
        [self styleContainer];
        
        [self createLetters];
        
    }
    return self;
}

#pragma - touch events handling

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    //need to call super so UIControl events can be dispatched for other touch events
    [super touchesBegan:touches withEvent:event];
    
    self.layer.backgroundColor = BG_COLOR_ROLLOVER;
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    
    self.layer.backgroundColor = BG_COLOR_DEFAULT;
}


#pragma -public methods

- (void)resetLetters
{
    [self createLetters];
}

- (void)setMissedLetter:(NSString *)aLetter
{
    //set the currentLetter prop
    self.currentLetter = aLetter;
    self.correctLetter = NO;
    
    //animate backgroundColor
    [AnimationUtil animateLayer:self.layer withColor:BG_COLOR_WRONG_GUESS toDelegate:self];
}

- (void)setGuessedLetter:(NSString *)aLetter
{
    //set the currentLetter prop
    self.currentLetter = aLetter;
    self.correctLetter = YES;
    
    //animate backgroundColor
    [AnimationUtil animateLayer:self.layer withColor:BG_COLOR_CORRECT_GUESS toDelegate:self];
    
}

//animation delegate
- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    if (self.correctLetter)
    {
        [self changeLetterState:self.currentLetter toState:GuessedLetterState];
    } else {
        [self changeLetterState:self.currentLetter toState:MissedLetterState];
    }
    
}




@end
