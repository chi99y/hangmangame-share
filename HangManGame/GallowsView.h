//
//  GallowsView.h
//  HangMan
//
//  Created by Chin S. Huang on 2/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GallowsView : UIView

@property (nonatomic) CGPoint hangPoint;//point at which the man will join the head

@end
