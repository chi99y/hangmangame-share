//
//  HangManViewController.m
//  HangMan
//
//  Created by Chin S. Huang on 2/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HangManViewController.h"
#import "GallowsView.h"
#import "HangingManView.h"
#import "HangManGame.h"
#import "CustomButton.h"
#import "SecretWordChooserViewController.h"
#import "LetterPickerViewController.h"
#import "SecretWordDisplayView.h"
#import "GameOverViewController.h"
#import "SecretWordDisplayView.h"
#import "GuessLetterContainerView.h"

@interface HangManViewController()<SecretWordChooserViewControllerDelegate, LetterPickerViewDelegate,HangingManViewDataSource,GameOverViewControllerDelegate>

//Model
@property (nonatomic, strong) HangManGame *hangManGame;

//Views
@property (nonatomic, weak) IBOutlet GallowsView *gallowsView;
@property (nonatomic, weak) IBOutlet HangingManView *hangingManView;
@property (nonatomic, weak) IBOutlet CustomButton *startButton;
@property (weak, nonatomic) IBOutlet SecretWordDisplayView *secretWordDisplayView;
@property (nonatomic, weak) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet GuessLetterContainerView *guessLetterView;

@property (nonatomic) NSUInteger hangmanStage;
@property (nonatomic) SecretWordChooserMode wordChooserMode;
@property (nonatomic, weak) NSString * sequeInProgress;


@end

//private constants
CGPoint const GALLOWS_PORTRAIT= {217.0f, 210.0f};
CGPoint const GALLOWS_LANDSCAPE= {400.0f, 135.0f};
NSString *const STARTGAME_MESSAGE = @"Start by setting your secret word";
NSString *const GAME_IN_PROGRESS_MESSAGE = @"Click box below to guess word";
NSString *const SEQUE_SECRETWORD_CHOOSER = @"Secret Word Chooser";
NSString *const SEQUE_LETTER_PICKER = @"Letter Picker";
NSString *const SEQUE_GAME_OVER = @"Game Over";


@implementation HangManViewController

@synthesize startButton = _startButton, messageLabel = _messageLabel, gallowsView = _gallowsView, hangingManView = _hangingManView, hangManGame = _hangManGame,secretWordDisplayView = _secretWordDisplayView, guessLetterView = _guessLetterView, hangmanStage = _hangmanStage, wordChooserMode = _wordChooserMode, sequeInProgress = _sequeInProgress;

#pragma mark - getter / setters

- (void)setHangmanStage:(NSUInteger)hangmanStage
{
    _hangmanStage = hangmanStage;
    [self.hangingManView setNeedsDisplay];
}

#pragma mark - private methods

- (void)updateSecretWordDisplay
{
    if (self.secretWordDisplayView)
    {
        //update string from model
        if (self.hangManGame.gameResult == GameOver || self.hangManGame.gameResult == YouWin)
        {
            //just show the secret word when game is over
            [self.secretWordDisplayView displaySecretWordWith:[self.hangManGame.secretWord wordAsString]];
            
        } else {
            
            [self.secretWordDisplayView displaySecretWordWith:[self.hangManGame getGuessedLettersAsString]];
        }
        
    }
}

- (void)updateHangingManView
{
    self.hangmanStage = [self.hangManGame missed];
    [self.hangingManView setNeedsDisplay];
}

- (void)updateGuessLetterViewWithGuess:(BOOL)correct
{
    if (correct)
    {
        [self.guessLetterView setGuessedLetter:[[self.hangManGame guessedLetters] lastObject]];
    } else {
        [self.guessLetterView setMissedLetter:[self.hangManGame currentMissedLetter]]; 
    }
    
}

- (void)updateModelWithWrongGuess
{
    //update hangman view
    self.hangmanStage = [self.hangManGame missed];
    [self.hangingManView setNeedsDisplay];
}

#pragma mark - Game States

- (void)setup
{
    //initialization
    [self.startButton setTitle:@"Start Game" forState:UIControlStateNormal];
    self.messageLabel.text = STARTGAME_MESSAGE;
     self.wordChooserMode = ChooseSecretWord;
    
    //set hangman delegate
    self.hangingManView.dataSource = self;
    self.hangmanStage = ALLOWED_MISSES + 1;//show the full hanging man upon startup
    
    //hide these views
    self.guessLetterView.hidden = YES;
}

- (void)newGameIsStarting
{
    //hide the button
    self.startButton.enabled = NO;
    self.startButton.hidden = YES;
    
    //show the views
    self.guessLetterView.hidden = NO;
    [self updateSecretWordDisplay];
    [self updateHangingManView];
    self.messageLabel.text = GAME_IN_PROGRESS_MESSAGE;
    
    //reset
    [self.guessLetterView resetLetters];
    
}

- (void)gameHasEnded
{
    //hide these views
    self.startButton.enabled = YES;
    self.guessLetterView.hidden = YES;
    [self.secretWordDisplayView clearDisplay];
    
    //show these views
    self.startButton.hidden = NO;
    self.messageLabel.text = STARTGAME_MESSAGE;
    
    //show the full hanging man at end
    self.hangmanStage = ALLOWED_MISSES + 1;
    
    //reset
    self.wordChooserMode = ChooseSecretWord;
    
}


#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self setup];
}

- (void)viewDidUnload
{
    [self setGallowsView:nil];
    [self setHangingManView:nil];
    [self setStartButton:nil];
    [self setMessageLabel:nil];
    [self setSecretWordDisplayView:nil];
    [self setGuessLetterView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

//we can use this method to detect when dismissModalViewControllerAnimated is complete
- (void)viewDidAppear:(BOOL)animated
{
    //update views after Secret Word Chooser seque completes
    if ([self.sequeInProgress isEqualToString:SEQUE_SECRETWORD_CHOOSER] && self.wordChooserMode == GuessSecretWord)
    {
        //check model and update view accordingly
        if (self.hangManGame.gameResult == YouWin)
        {
            //show the secret word
            [self updateSecretWordDisplay];
            
            [self.secretWordDisplayView showCorrectGuessAnimation];
            
            [self performSelector:@selector(performSegueWithIdentifier:sender:) withObject:SEQUE_GAME_OVER afterDelay:1.0];
            
        } else {
          
            [self updateModelWithWrongGuess];
            [self.secretWordDisplayView showInCorrectGuessAnimation];
        }
    }
    
    //update views after Letter Picker seque completes
    if ([self.sequeInProgress isEqualToString:SEQUE_LETTER_PICKER])
    {
        int result = self.hangManGame.gameResult;
        
        if (result == GuessCorrect)
        {
            [self updateSecretWordDisplay];
            [self.secretWordDisplayView showCorrectGuessAnimation];
            [self updateGuessLetterViewWithGuess:YES];
            
        } else if (result == GuessIncorrect){

            [self updateModelWithWrongGuess];
            [self updateGuessLetterViewWithGuess:NO];
                   
        } else if (result == GameOver)
        {
            self.hangmanStage = GameOver;
            [self updateHangingManView];
            [self performSelector:@selector(performSegueWithIdentifier:sender:) withObject:SEQUE_GAME_OVER afterDelay:.5];
            
        } else if (result == YouWin)
        {
            //update display
            [self updateSecretWordDisplay];
            [self performSelector:@selector(performSegueWithIdentifier:sender:) withObject:SEQUE_GAME_OVER afterDelay:.5];
        }

    }
    
    self.sequeInProgress = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait || 
        toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        //move gallows to portrait position 
        self.gallowsView.center = GALLOWS_PORTRAIT;
    }
    else 
    {
        //move gallows to landscape position
        self.gallowsView.center = GALLOWS_LANDSCAPE;
    }
    
    //move hanging man to where the hangPoint is
    CGPoint hPoint = [self.gallowsView convertPoint:self.gallowsView.hangPoint toView:self.view];
    CGPoint offsetPoint = CGPointMake(hPoint.x, (hPoint.y + self.hangingManView.bounds.size.height/2));
    self.hangingManView.center = offsetPoint;
    
    //NSLog(@"gallows :: hangPoint.x=%f, y=%f", hPoint.x, hPoint.y);
}

#pragma mark - View Actions -

- (IBAction)startGameAction:(CustomButton *)sender 
{
    NSLog(@"perform seque for Secret Word :: %@", sender);
    [self performSegueWithIdentifier:SEQUE_SECRETWORD_CHOOSER sender:self];
}

- (IBAction)guessWordAction:(id)sender 
{
    NSLog(@"SecretWordDisplayView has been clicked!");
    self.wordChooserMode = GuessSecretWord;
    [self performSegueWithIdentifier:SEQUE_SECRETWORD_CHOOSER sender:self];
}

- (IBAction)pickLetterAction:(id)sender 
{
    [self performSegueWithIdentifier:SEQUE_LETTER_PICKER sender:self];
}


#pragma mark - Implement HangManView dataSource (delegate)

-( NSUInteger)getCurrentHangStage:(HangingManView *)sender
{
    return self.hangmanStage;
}


#pragma mark - seques

// prepare for the modal view controller buttons in the toolbar 

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier hasPrefix:SEQUE_SECRETWORD_CHOOSER]) {
        SecretWordChooserViewController *secret = (SecretWordChooserViewController *)segue.destinationViewController;
        secret.delegate = self;
        secret.viewMode = self.wordChooserMode;
    } 
    else if ([segue.identifier hasPrefix:SEQUE_LETTER_PICKER]) 
    {
        LetterPickerViewController *picker = (LetterPickerViewController *)segue.destinationViewController;
        picker.delegate = self;
        
    }
    else if ([segue.identifier hasPrefix:SEQUE_GAME_OVER]) 
    {
        GameOverViewController *gameover = (GameOverViewController *)segue.destinationViewController;
        gameover.delegate = self;
        gameover.viewMode = self.hangManGame.gameResult == YouWin? ShowWinView : ShowLoseView;
        
    } 
    
    //set prop
    self.sequeInProgress = segue.identifier;
}



#pragma mark --implementing SecretWordViewControllerDelegate protocol--

- (void)secretWordController:(id)sender didSetSecretWord:(SecretWord *)theWord
{
    //start a game model by initializing a new game with a copy of the SecretWord object
    self.hangManGame = [[HangManGame alloc] initGameWithSecretWord:[theWord copy]];
    
    NSLog(@"SecretWord Delegate called :: secretWord = %@", [self.hangManGame getGuessedLettersAsString] );
    
    //return from modal
    [self dismissModalViewControllerAnimated:YES];
    
    //set game state
    [self newGameIsStarting];
}

- (void)secretWordController:(id)sender didSetGessedWord:(NSString *)theWord
{
    NSLog(@"SecretWord Delegate called :: guessedWord = %@", theWord );
    
    //guess the word
    [self.hangManGame guessTheWord:theWord];

    //return from modal
    [self dismissModalViewControllerAnimated:YES];
}



#pragma mark --implementing LetterPickerViewDelegate protocol--

- (void)letterPickerView:(id)sender didSelect:(NSString *)aLetter
{
    [self.hangManGame guessALetter:aLetter];
        
    [self dismissModalViewControllerAnimated:YES];
}

- (NSArray *)setRemainingLettersFor:(id)sender
{
    return [self.hangManGame remainingLetters];
}

#pragma mark --implementing GameOverControllerViewDelegate protocol--

- (void)gameOverViewDidRestartGame:(id)sender
{
    [self dismissModalViewControllerAnimated:YES];
    
    //game state
    [self gameHasEnded];
}


@end



