//
//  LetterPickerViewController.h
//  HangMan
//
//  Created by Chin S. Huang on 2/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LetterPickerViewDelegate <NSObject>

- (void)letterPickerView:(id)sender didSelect:(NSString *)aLetter;

- (NSArray *)setRemainingLettersFor:(id)sender;

@end

@interface LetterPickerViewController : UIViewController<UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic, strong) NSString *selectedLetter;

@property (nonatomic, weak) id<LetterPickerViewDelegate>delegate;

@end
