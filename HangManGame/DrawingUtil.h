//
//  DrawingUtil.h
//  HangManGame
//
//  Created by Chin S. Huang on 3/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef struct {
    float textWidth;
    float textPosX;
} DrawTextProps;

@interface DrawingUtil : NSObject

+ (DrawTextProps)drawTextInContext:(CGContextRef)context inRect:(CGRect)rect withText:(NSString *)aString withFontSize:(CGFloat)fontSize letterSpacing:(CGFloat)spacing color:(UIColor *)color drawingMode:(CGTextDrawingMode)mode;

@end
