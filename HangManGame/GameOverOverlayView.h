//
//  GameOverOverlayView.h
//  HangManGame
//
//  Created by Chin S. Huang on 3/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DrawStarToLayerDelegate : NSObject 
@end

@interface DrawImageToLayerDelegate : NSObject 
@end

@interface GameOverOverlayView : UIView

- (void)displayWinningGraphics;

- (void)displayLosingGraphics;

@end
