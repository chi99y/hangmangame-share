//
//  GallowsView.m
//  HangMan
//
//  Created by Chin S. Huang on 2/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GallowsView.h"

//constant
CGPoint const HANGPOINT= {5.0f, 60.0f};

@implementation GallowsView

@synthesize hangPoint = _hangPoint;

- (void)setup
{
    self.hangPoint = HANGPOINT;
}

- (void)awakeFromNib
{
    [self setup];
}

- (void)drawGallows
{
    //create context
    CGContextRef gallowsContext = UIGraphicsGetCurrentContext();
    
    //create connecting points
    CGPoint addLines[] =
    {
        self.hangPoint,
        CGPointMake(5.0, 5.0),
        CGPointMake(80.0, 5.0),
        CGPointMake(80.0, 260.0),
        CGPointMake(50.0, 260.0),
        CGPointMake(110.0, 260.0)
    };
    
    //add lines to paths
    CGContextAddLines(gallowsContext, addLines, sizeof(addLines)/sizeof(addLines[0]));
       
    //config the lines then stroke the path
    CGContextSetLineWidth(gallowsContext, 5.0);
    [[UIColor blueColor] setStroke];
    CGContextStrokePath(gallowsContext);
}


- (void)drawRect:(CGRect)rect
{
    [self drawGallows];
}


- (NSString *)description
{
    return @"[GallowsView: Display the Gallows with a hook point]";
}

@end
