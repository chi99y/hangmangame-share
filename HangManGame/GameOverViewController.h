//
//  GameOverViewController.h
//  HangManGame
//
//  Created by Chin S. Huang on 3/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GameOverViewControllerDelegate <NSObject>

- (void)gameOverViewDidRestartGame:(id)sender;

@end

//enums
typedef enum {
    ShowWinView,
    ShowLoseView
} GameOverMode;

@interface GameOverViewController : UIViewController

@property (nonatomic) GameOverMode viewMode;

@property (nonatomic, weak) id<GameOverViewControllerDelegate>delegate;

@end
