//
//  GuessLetter.h
//  HangManGame
//
//  Created by Chin S. Huang on 3/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

//enums
typedef enum {
    RemainingLetterState,
    MissedLetterState,
    GuessedLetterState
} GuessLetterState;

@interface GuessLetter : UIView

@property (nonatomic, strong, readonly) NSString *letter;
@property (nonatomic) GuessLetterState letterState;

- (id)initWithLetter:(NSString *)aLetter inFrame:(CGRect)frame;
- (void)setState:(GuessLetterState)state;

@end

@interface DrawTextToLayerDelegate : NSObject

- (id)initWithLetter:(NSString *)aLetter;

@end
