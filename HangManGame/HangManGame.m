//
//  HangManGame.m
//  HangMan
//  -Model for the HangMan game.
//
//  Created by Chin S. Huang on 2/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HangManGame.h"

//constants
NSUInteger const ALLOWED_MISSES = 5;
NSString *const DELIMITER_FOR_REMAINING_LETTERS = @"*";

@interface HangManGame()
@property (nonatomic, strong) NSMutableArray *letterOccurrences;//Array holding indexes as NSSetIndexSet
@end

@implementation HangManGame

//public properties
@synthesize secretWord = _secretWord, matchedLetterPositions = _matchedLetterPositions, remainingLetters = _remainingLetters, 
guessedLetters = _guessedLetters, currentMissedLetter = _currentMissedLetter, missed = _missed, isGameOver = _isGameOver, gameResult = _gameResult;

//private properties
@synthesize letterOccurrences = _letterOccurrences;

#pragma mark - private methods

- (BOOL)validateLetter:(NSString *)aLetter
{
    if ( aLetter == nil ||
        [aLetter length] > 1 ||
        [SecretWord testIfLetterIsPermissible:aLetter]== NO)
    {
        return NO;
    }
        
    return YES;
}

- (NSArray *)letterOccurrencesAsArray
{
    NSMutableIndexSet *aggregateSet = [[NSMutableIndexSet alloc] init];
    
    //Enumerations using NSEnumerator
    NSEnumerator *indexEnumerator = [self.letterOccurrences objectEnumerator];
    id indexObj;
    while (indexObj = [indexEnumerator nextObject]) {
        [aggregateSet addIndexes:(NSIndexSet *)indexObj];
    }
    
    NSMutableArray *guessedSoFar = [[NSMutableArray alloc] init];
    
    for (int i=0; i<[[self.secretWord wordAsString] length]; i++) {
        NSString *targetLetter = [[self.secretWord wordAsString] substringWithRange:NSMakeRange(i,1)];
        if ([aggregateSet containsIndex:i])
        {
            //add the matched letter to string
            [guessedSoFar addObject:targetLetter];
        } else {
            //add underscore _ to string representing letters not guessed yet
            [guessedSoFar addObject:DELIMITER_FOR_REMAINING_LETTERS];
        }
    }
    
    return [guessedSoFar copy];//return an immutable copy
}

- (void)logLetterMatches:(NSString *)letter
{
    //get the indexes from the reference array and store is as a set in an array
    NSMutableIndexSet *mSet = [[NSMutableIndexSet alloc] init]; 
    
    NSArray *wordArray = [self.secretWord wordAsArray];
    for (int i=0; i < [wordArray count]; i++)
    {
        if ( [letter isEqualToString:[wordArray objectAtIndex:i]] )
        {
            [mSet addIndex:i];
        }
    }
    
    //save it to an array
    NSIndexSet *imSet = [mSet mutableCopy];
    [self.letterOccurrences addObject:imSet];
    //NSLog(@"HangManGame :: logLetterMatches : guess[%@] = numberOfIndexes [%i]",letter,[imSet count]);
    
}

- (void)setup
{
    self.letterOccurrences = [[NSMutableArray alloc] init];
    _remainingLetters = [[NSMutableArray alloc] initWithArray:[SecretWord permissibleLetters] copyItems:YES];
    _guessedLetters = [[NSMutableArray alloc] init];
}


#pragma mark - public methods

+ (NSString *)stringFromArray:(NSArray *)anArray
{
    //use Fast Enumerations
    NSString *string = [[NSString alloc] init];
    
    for (NSString *element in anArray) {
        string = [string stringByAppendingString:element];
    }
    return string;
}

//designated initializer
- (id)initGameWithSecretWord:(SecretWord *)secretWord
{
    if (self = [super init])
    {
        if (secretWord) {
            _secretWord = secretWord;
        } else {
            _secretWord = [[SecretWord alloc] init];
        }
        
        [self setup];
    }

    return self;
}

//override init
- (id)init
{
    return [self initGameWithSecretWord:nil];
}


- (GuessResult)guessALetter:(NSString *)aLetter
{
    //validate first
    if ([self.secretWord wordAsString]==nil) return SecretWordNotSet;
    if (self.isGameOver) return GameOver;
    
    //validate and check if matches
    if ( [self validateLetter:aLetter] == NO ) 
    {
        return InvalidGuess;
    }
    
    //check the valid letter against secret word
    NSString *upLetter = [aLetter uppercaseString];
    
    //update properties for the guess
    [_guessedLetters addObject:upLetter];
    [_remainingLetters removeObject:upLetter];
    
    int result;
    if (![self.secretWord.wordAsArray containsObject:upLetter])
    {
        //log a miss
        _currentMissedLetter = upLetter;
         _missed++;
        if(_missed > ALLOWED_MISSES) 
        {
            _isGameOver = YES;
            return self.gameResult = GameOver;  
        }
        result = self.gameResult = GuessIncorrect;
        
    } else {
        
        //log letter[s] that was a match
        [self logLetterMatches:upLetter];
        
        //check to see if all the letters match by checking whether
        //it still contains the delimiter symbol
        if ([[self letterOccurrencesAsArray] containsObject:DELIMITER_FOR_REMAINING_LETTERS] == NO)
        {
            result = self.gameResult = YouWin;
        } else {
            result = self.gameResult = GuessCorrect;
        }
    }
    
    return result;
}

- (GuessResult)guessTheWord:(NSString *)aWord
{
    if ([self.secretWord wordAsString]==nil) return SecretWordNotSet;
    if (self.isGameOver) return GameOver;
    
    NSString *upWord = [aWord uppercaseString];
    
    if (![upWord isEqualToString:[self.secretWord.wordAsString uppercaseString]])
    {
        
        ++_missed;
        if(_missed > ALLOWED_MISSES) 
        {
            _isGameOver = YES;
            
            return self.gameResult = GameOver;  
        }
        
        return GuessIncorrect;
    }
    
    return self.gameResult = YouWin;
}

- (NSString *)getGuessedLettersAsString
{
    NSString *guessedSoFar = [HangManGame stringFromArray:[self letterOccurrencesAsArray]];
                              
    guessedSoFar = [guessedSoFar stringByReplacingOccurrencesOfString:DELIMITER_FOR_REMAINING_LETTERS withString:@"_ "];
    
    NSLog(@"HangManGame :: getGuessedLettersAsString = %@",guessedSoFar);
    return guessedSoFar;
}

- (NSString *)getRemainingLettersAsString
{
    return [HangManGame stringFromArray:[self.remainingLetters copy]];
}


@end
