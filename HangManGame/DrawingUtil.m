//
//  DrawingUtil.m
//  HangManGame
//
//  Created by Chin S. Huang on 3/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DrawingUtil.h"

@implementation DrawingUtil

+ (DrawTextProps)drawTextInContext:(CGContextRef)context inRect:(CGRect)rect withText:(NSString *)aString withFontSize:(CGFloat)fontSize letterSpacing:(CGFloat)spacing color:(UIColor *)color drawingMode:(CGTextDrawingMode)mode
{
    UIGraphicsPushContext(context);
    
    const char *word = [aString cStringUsingEncoding:NSUTF8StringEncoding];
    
    //correct coordinate system to iOS
    CGContextSetTextMatrix(context, CGAffineTransformMake(1.0, 0.0, 0.0, -1.0, 0.0, 0.0));
    
    //font and spacing
    CGContextSelectFont(context, "Helvetica-Bold", fontSize, kCGEncodingMacRoman);
    CGContextSetCharacterSpacing (context, spacing); 
    
    //Measure the text's width - This involves drawing an invisible string to calculate the X position difference
    DrawTextProps textProps;
   
    //the 5 step needed to calculate width of text using Quartz 2D
    CGPoint start = CGContextGetTextPosition(context);
    CGContextSetTextDrawingMode (context, kCGTextInvisible); 
    CGContextShowText(context, word, [aString length]);
    CGPoint final =  CGContextGetTextPosition(context);
    textProps.textWidth = final.x - start.x;
    
    //calculate centering of text block
    textProps.textPosX = (rect.size.width - textProps.textWidth) / 2;
    
    //styling
    CGContextSetTextDrawingMode (context, mode); 
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextSetShadow(context, CGSizeMake(.75f, 2.5f), 1.0);
    
    CGContextShowTextAtPoint(context, textProps.textPosX, CGRectGetMidY(rect), word, [aString length]);
    UIGraphicsPopContext();
    
    return textProps;
}


@end
