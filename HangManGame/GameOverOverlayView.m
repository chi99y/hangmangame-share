//
//  GameOverOverlayView.m
//  HangManGame
//
//  Created by Chin S. Huang on 3/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GameOverOverlayView.h"
#import <QuartzCore/QuartzCore.h>
#import "DrawingUtil.h"

@interface GameOverOverlayView()
@property (nonatomic, strong) DrawStarToLayerDelegate *starLayerDelegate;
@property (nonatomic, strong) DrawImageToLayerDelegate *imageLayerDelegate;
@property (nonatomic) BOOL isWin;
@end

//constants
NSString *const YOU_WIN_TEXT = @"YOU WIN!";

@implementation GameOverOverlayView

@synthesize starLayerDelegate = _starLayerDelegate, imageLayerDelegate = _imageLayerDelegate, isWin = _isWin;

#define STAR_RADIUS 20
#define LAYER_WIDTH 300

const CGPoint starPoints[] = {
    {150, 43},  {225, 75},
    {255, 150}, {225, 225},
    {150, 254}, {75, 225},
    {45, 150},  {75, 75}
};


- (void)drawAStarLayerInto:(CALayer *)aParentLayer movedTo:(CGPoint)point
{
    //add star layers
    CALayer *starLayer1 = [CALayer layer];
    starLayer1.frame = CGRectMake(0, 0, STAR_RADIUS*2, STAR_RADIUS*2);
    starLayer1.anchorPoint = CGPointMake(0.5, 0.5);
    starLayer1.position = point;
    starLayer1.delegate = self.starLayerDelegate;
    [starLayer1 setNeedsDisplay];
    [aParentLayer addSublayer:starLayer1];
}


- (void)rotateALayer:(CALayer *)aLayer
{
    CABasicAnimation *rotate = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    rotate.fromValue = [NSNumber numberWithFloat:0];
    rotate.toValue = [NSNumber numberWithFloat:M_PI / 2.0];
    rotate.duration = 0.5;
    rotate.repeatCount= HUGE_VAL;
    [aLayer addAnimation:rotate forKey:nil]; // "key" is optional
}



- (void)animateScaleOfLayer:(CALayer *)aLayer
{
    CABasicAnimation *scale = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    scale.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    scale.fromValue = [NSNumber numberWithFloat:1.0];
    scale.toValue = [NSNumber numberWithFloat:.75];
    scale.autoreverses = YES;
    scale.duration = 0.5;
    scale.repeatCount = HUGE_VAL;
    [aLayer addAnimation:scale forKey:nil];
}

- (void)drawWinningGraphics
{
    //main layer of the view
    CALayer *rootLayer = self.layer;
    
    CALayer *winningLayer = [CALayer layer];
    winningLayer.frame = CGRectMake(0, 0, LAYER_WIDTH, LAYER_WIDTH);//square
    winningLayer.anchorPoint = CGPointMake(.5, .5);
    winningLayer.position = CGPointMake(CGRectGetMidX(self.frame), (self.frame.size.height/2) - 10);
    [rootLayer addSublayer:winningLayer];
    
    //add all the stars
    for (int i=0; i< sizeof(starPoints) / sizeof(struct CGPoint); i++) {
        
        [self drawAStarLayerInto:winningLayer movedTo:starPoints[i]];
        
    }
    
    //animate the layer
    [self rotateALayer:winningLayer];

}

- (void)drawLosingGraphics
{
    //main layer of the view
    CALayer *rootLayer = self.layer;
    CALayer *losingLayer = [CALayer layer];
    
    losingLayer.frame = CGRectMake(0, 0, LAYER_WIDTH, LAYER_WIDTH);
    losingLayer.anchorPoint = CGPointMake(.5, .5);
    losingLayer.position = CGPointMake(CGRectGetMidX(self.frame), (self.frame.size.height/2) - 10);
    losingLayer.delegate = self.imageLayerDelegate;
    [losingLayer setNeedsDisplay];
    [rootLayer addSublayer:losingLayer];
    
    //animmate the layer
    [self animateScaleOfLayer:losingLayer];
}

- (void)setup
{
    // create an instance of the delegate to handle drawing a star to the layer
    self.starLayerDelegate = [[DrawStarToLayerDelegate alloc] init];
    self.imageLayerDelegate = [[DrawImageToLayerDelegate alloc] init];
}

- (void)awakeFromNib
{ 
    [self setup];
}


//Main drawing methods
- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    NSString *textToShow = (self.isWin)? YOU_WIN_TEXT : @"";
    
    //use the DrawingUtils to draw text into context
    [DrawingUtil drawTextInContext:context 
                            inRect:rect 
                          withText:textToShow 
                      withFontSize:25 
                     letterSpacing:4 
                             color:[UIColor redColor] 
                       drawingMode:kCGTextFillStroke];

}

#pragma -pubic methods

- (void)displayWinningGraphics
{
    self.isWin = YES;
    
    [self drawWinningGraphics];
}

- (void)displayLosingGraphics
{
    self.isWin = NO;
    
    [self drawLosingGraphics];
}

@end


//Helper class to act as a delegate for drawing a star into a layer
@implementation DrawStarToLayerDelegate

#pragma -drawinging delegation

-(void)drawStarToContext:(CGContextRef)context at:(CGPoint)center radius:(CGFloat)radius angle:(CGFloat)angle
{
    UIGraphicsPushContext(context);
    
	CGFloat x = radius * sinf(angle * M_PI / 5.0) + center.x;
	CGFloat y = radius * cosf(angle * M_PI / 5.0) + center.y;
	CGContextMoveToPoint(context, x, y);
	for(int i = 1; i < 5; ++i)
	{
		CGFloat x = radius * sinf((i * 4.0 * M_PI + angle) / 5.0) + center.x;
		CGFloat y = radius * cosf((i * 4.0 * M_PI + angle) / 5.0) + center.y;
		CGContextAddLineToPoint(context, x, y);
	}
	// And close the subpath.
	CGContextClosePath(context);
    
    //fill the star
    CGContextSetFillColorWithColor(context, [UIColor blueColor].CGColor);
    CGContextFillPath(context);
    
    UIGraphicsPopContext();
}

- (void)drawLayer:(CALayer *)layer inContext:(CGContextRef)ctx
{
    CGPoint layerMidPoint = CGPointMake(CGRectGetMidX(layer.bounds), CGRectGetMidY(layer.bounds));
    [self drawStarToContext:ctx at:layerMidPoint radius:STAR_RADIUS angle:0];
}

@end


//Helper class to act as a delegate for drawing an image into a layer
//--this way we can apply more advanced drawing functions like filters and such.
@implementation DrawImageToLayerDelegate

- (void)drawLayer:(CALayer *)layer inContext:(CGContextRef)ctx
{
    //correct the coordinate system for iOS
    CGContextTranslateCTM(ctx, 0.0, layer.frame.size.height);
    CGContextScaleCTM(ctx, 1.0, -1.0);
    //load image
    UIImage *image = [UIImage imageNamed:@"gameover_lose.png"];
    //draw image to context
    CGContextDrawImage(ctx, CGRectMake(0, 0, LAYER_WIDTH, LAYER_WIDTH), image.CGImage);
    
}
@end
