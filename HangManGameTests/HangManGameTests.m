//
//  HangManGameTests.m
//  HangManGameTests
//
//  Created by Chin S. Huang on 2/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HangManGameTests.h"
#import "HangManGame.h"
#import "SecretWord.h"

@implementation HangManGameTests

//test Class methods
- (void)testStringFromArray
{
    NSArray *anArray = [[NSArray alloc] initWithObjects:@"h",@"e",@"l",@"l",@"o", nil];
     STAssertEqualObjects([HangManGame stringFromArray:anArray], @"hello" , nil);
}

//new game should reset values
- (void)testInitNewGame
{
    HangManGame *game = [[HangManGame alloc] init];
    
    //check that we have new secretword without word set
    STAssertNil([game.secretWord wordAsString], nil);
    
    //check remainingLetters should be same as entire alphabet
    STAssertTrue([game.remainingLetters isEqualToArray:[SecretWord permissibleLetters]], nil);
    
    //check guessedLetters should be empty
    STAssertFalse(game.guessedLetters.count > 0, nil);
    
    //check missed is reset to 0
    STAssertTrue([game missed]==0, nil);
    
}

- (void)testGuessingALetter
{
    HangManGame *game = [[HangManGame alloc] init];
    
    //check return value for not setting secret word
    STAssertTrue([game guessALetter:@"chin"]==SecretWordNotSet,nil);
    
    [game.secretWord setSecretWord:@"HelloWorld"];
    
    //guess should not be more than 1 letter
    STAssertTrue([game guessALetter:@"chin"]==InvalidGuess, nil);
    
    //guess incorrectly should log 1 miss
    [game guessALetter:@"z"];
    STAssertTrue([game missed]==1, nil);
    
    //guess correctly and incorrectly should return appropriate results
    STAssertTrue([game guessALetter:@"l"]==GuessCorrect, nil);
    STAssertTrue([game guessALetter:@"z"]==GuessIncorrect, nil);
    
}

- (void)testGuessedLetters
{
     HangManGame *game = [[HangManGame alloc] init];
    
    [game.secretWord setSecretWord:@"HelloWorld"];
    [game guessALetter:@"l"];
    
    //should match the case of the original inputted secret word
    STAssertEqualObjects([game getGuessedLettersAsString], @"_ _ ll_ _ _ _ l_ ", nil);
    STAssertFalse([game getGuessedLettersAsString] == @"_ _ LL_ _ _ _ L_ ", nil);
    
    //test remaining letters
    STAssertEqualObjects([game getRemainingLettersAsString],@"ABCDEFGHIJKMNOPQRSTUVWXYZ", nil);
}

- (void)testGuessingTheWord
{
    HangManGame *game = [[HangManGame alloc] init];
    
    [game.secretWord setSecretWord:@"HelloWorld"];
    
    //guess correctly and incorrectly should return appropriate results
    STAssertTrue([game guessTheWord:@"theWrongWord"]==GuessIncorrect,nil);
    STAssertTrue([game guessTheWord:@"HelloWorld"]==YouWin, nil);
    
}




@end
