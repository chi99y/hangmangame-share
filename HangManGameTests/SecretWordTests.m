//
//  SecretWordTests.m
//  HangManGame
//
//  Created by Chin S. Huang on 2/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SecretWordTests.h"
#import "SecretWord.h"

#import <UIKit/UIKit.h>
//#import "application_headers" as required

@implementation SecretWordTests

- (void)testPermissibleLetters
{
    //should be the alphabet
    NSArray *alphabetArray = [[NSArray alloc] initWithObjects:
                              @"A",@"B",@"C",@"D",@"E",@"F",@"G",@"H",@"I",@"J",@"K",@"L",@"M",
                              @"N",@"O",@"P",@"Q",@"R",@"S",@"T",@"U",@"V",@"W",@"X",@"Y",@"Z",
                              nil];
    STAssertTrue([alphabetArray isEqualToArray:[SecretWord permissibleLetters]] ,@"permissible letters don't match alphabet in uppercase");
}

//test secret word setting
- (void)testSettingSecretWord
{
    SecretWord *secret = [[SecretWord alloc] init];
    //test min value < 2
    NSString *minWord= @"I";
    STAssertFalse([secret setSecretWord:minWord], nil);
    //test max value > 20
    NSString *maxWord= @"abcdefghijklmnopqrstuvwxyz";
    STAssertFalse([secret setSecretWord:maxWord], nil);
    //set and check invalid char
    NSString *badCharWord= @"chin@home";
    STAssertFalse([secret setSecretWord:badCharWord], nil);
    //check valid word
    NSString *validWord= @"Programming";
    STAssertTrue([secret setSecretWord:validWord], nil);
    //should return valid word
    STAssertEqualObjects(validWord, [secret wordAsString], nil);
    //the array with the words should be all caps
    NSMutableArray *refArr = [[NSMutableArray alloc] initWithObjects:@"P",@"R",@"O",@"G",@"R",@"A",@"M",@"M",@"I",@"N",@"G",nil];
    STAssertEqualObjects([secret wordAsArray], refArr, nil);
    
}

@end
