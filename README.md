//-----------------------------------------------------------------------
// 	HangManGame for iPhone
//  
//	Date: 2.23.2012
//	Author: Chin S. Huang / email: chin.cog@gmail.com	 
//-----------------------------------------------------------------------


Description:
-----------
This project was created for the purpose of learning basic iOS 5 development.

The game is based on the classic children's guessing game "Hang Man".  The 
initial version of the application is very limited in scope and functionality
making it more as a demo than a real game.  Perhaps over time, contributors 
can add more interesting functionality to a point where it can be submitted as
a real application in the AppStore.

Learning topics covered in app so far:
- C and Objective-C programming 
- iOS version of MVC (controller heavy)
- Custom UIView
- Quartz2D and custom drawing for graphics and text
- CALayers and animations
- Seques and modal view controllers
- Delegation and protocols
- iOS unit testing

Version: 1.0

Todos and feature ideas:
1. Clean up layouts for landscape orientation
2. Clean up info page
3. Design scoring system that can store user scores
4. Secret word loading from a web-based source
5. Secret word set from another iPhone
6. Better Hang Man graphics, perhaps with animation or physics
7. Possibly integrating camera pic of a face into Hang Man graphics.
